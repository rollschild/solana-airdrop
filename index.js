import {
  Connection,
  PublicKey,
  clusterApiUrl,
  Keypair,
  LAMPORTS_PER_SOL,
} from "@solana/web3.js";

// Keypair allows us to create a new wallet
const account = Keypair.generate();
// Each wallet has a PublicKey and a secret key

const publicKey = new PublicKey(account.publicKey);

const getAccountBalance = async () => {
  try {
    const connection = new Connection(clusterApiUrl("devnet"), "confirmed");
    const accountBalance = await connection.getBalance(publicKey);
    console.log("Account balance: ", accountBalance);
  } catch (error) {
    console.error(error);
  }
};

const airdropSol = async () => {
  try {
    const connection = new Connection(clusterApiUrl("devnet"), "confirmed");
    const fromAirdropSignature = await connection.requestAirdrop(
      publicKey,
      2 * LAMPORTS_PER_SOL,
    );
    await connection.confirmTransaction(fromAirdropSignature);
  } catch (error) {
    console.error(error);
  }
};

const main = async () => {
  await getAccountBalance();
  await airdropSol();
  await getAccountBalance();
};

main();
